#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QQmlContext>
#include <QColor>

int main(int argc, char *argv[])
{

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

//    QQuickView viwer;
//    viwer.setFlags(Qt::FramelessWindowHint);
//    viwer.setColor(QColor(Qt::transparent));
//    viwer.setSource(QUrl("qrc:/qml/main.qml"));
//    viwer.show();
//    viwer.rootContext()->setContextProperty("mainwindow",&viwer);

    return app.exec();
}

