import QtQuick 2.0
import QtGraphicalEffects 1.0
Rectangle {
    width: parent.width
    height: parent.height
    Rectangle{
        id: order_rec_id
        width:parent.width *0.6
        height: parent.height *0.4
        color: "black"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: parent.height*0.24
        Column{
            width: parent.width
            height: parent.height
            Text {
                text: qsTr("OrderID:")
                color: "white"
                anchors.top: parent.top
                anchors.topMargin: parent.height *0.2
                anchors.left: parent.left
                anchors.leftMargin: parent.width *0.2
                font.pointSize: parent.width *0.03
            }
            Text {
                text: qsTr("hotel:")
                color: "white"
                anchors.top: parent.top
                anchors.topMargin: parent.height *0.3
                anchors.left: parent.left
                anchors.leftMargin: parent.width *0.2
                font.pointSize: parent.width *0.03
            }
            Text {
                text: qsTr("reserve:")
                color: "white"
                anchors.top: parent.top
                anchors.topMargin: parent.height *0.4
                anchors.left: parent.left
                anchors.leftMargin: parent.width *0.2
                font.pointSize: parent.width *0.03
            }
            Text {
                text: qsTr("RoomNumb:")
                color: "white"
                anchors.top: parent.top
                anchors.topMargin: parent.height *0.5
                anchors.left: parent.left
                anchors.leftMargin: parent.width *0.2
                font.pointSize: parent.width *0.03
            }
            Text {
                text: qsTr("Check_in:")
                color: "white"
                anchors.top: parent.top
                anchors.topMargin: parent.height *0.6
                anchors.left: parent.left
                anchors.leftMargin: parent.width *0.2
                font.pointSize: parent.width *0.03
            }
            Text {
                text: qsTr("Check_out:")
                color: "white"
                anchors.top: parent.top
                anchors.topMargin: parent.height *0.7
                anchors.left: parent.left
                anchors.leftMargin: parent.width *0.2
                font.pointSize: parent.width *0.03
            }
            Text {
                text: qsTr("name:")
                color: "white"
                anchors.top: parent.top
                anchors.topMargin: parent.height *0.8
                anchors.left: parent.left
                anchors.leftMargin: parent.width *0.2
                font.pointSize: parent.width *0.03
            }
        }
    }
    DropShadow{
        anchors.fill: order_rec_id
        horizontalOffset: 7
        verticalOffset: 7
        radius: 8
        samples: 20
        color: "#999999"
        source: order_rec_id
    }
    Rectangle{
        id:order_line_id
        width:parent.width *0.8
        height:1
        anchors.bottom:parent.bottom
        anchors.bottomMargin:parent.height *0.3
        anchors.horizontalCenter: parent.horizontalCenter
    }
    LinearGradient{
        anchors.fill:order_line_id
        gradient: Gradient{
            GradientStop{position: 0.0; color: "white"}
            GradientStop{position: 0.3; color: "black"}
            GradientStop{position: 0.7; color: "black"}
            GradientStop{position: 1; color: "white"}
        }
        source: order_line_id
        start: Qt.point(0,0)
        end:Qt.point(order_line_id.width,0)
    }
    Rectangle{
        id:order_confirm_rec_id
        width:parent.width *0.2
        height:parent.height *0.05
        color: "black"
        anchors.bottom:parent.bottom
        anchors.bottomMargin:parent.height *0.2
        anchors.horizontalCenter: parent.horizontalCenter
        Text {
            text: qsTr("confirm")
            color: "white"
            font.pointSize: parent.width*0.1
            anchors.centerIn: parent
        }

    }
    DropShadow{
        anchors.fill: order_confirm_rec_id
        horizontalOffset: 5
        verticalOffset: 5
        radius: 8
        samples: 17
        color: "#999999"
        source: order_confirm_rec_id
    }
}
