import QtQuick 2.0
import QtQuick.Controls 1.5
import QtGraphicalEffects 1.0

Rectangle {
    width:parent.width
    height: parent.height
    radius:1000
    color:"white"
    Column{
        width: parent.width
        height: parent.height
        Text {
            id:handle_text_id
            text: qsTr("AINN")
            anchors.top: parent.top
            anchors.topMargin: parent.height * 0.2
            anchors.horizontalCenter: parent.horizontalCenter
            font.pointSize: parent.width *0.1
        }
        Rectangle{
            id:line_one_id
            width:parent.width *0.8
            height:1
            anchors.bottom: phonenum_text_id.bottom
            anchors.bottomMargin: -10
            anchors.left: parent.left
            anchors.leftMargin: parent.width *0.1
        }
        LinearGradient{
            anchors.fill:line_one_id
            gradient: Gradient{
                GradientStop{position: 0.0; color: "white"}
                GradientStop{position: 0.5; color: "black"}
                GradientStop{position: 0.99; color: "white"}
            }
            source: line_one_id
            start: Qt.point(0,0)
            end:Qt.point(line_one_id.width,0)
        }

        Text{
            id:phonenum_text_id
            text:"PhoneNumber: "
            anchors.top: parent.top
            anchors.topMargin: parent.height * 0.4
            anchors.left: parent.left
            anchors.leftMargin: parent.width * 0.1
            font.pointSize: parent.width *0.03
        }
        Row{
            width: parent.width
            height: parent.height
            Text {
                id: code_text_id
                text: "code:"
                anchors.top: parent.top
                anchors.topMargin: parent.height * 0.5
                anchors.left: parent.left
                anchors.leftMargin: parent.width * 0.1
                font.pointSize: parent.width *0.03
            }
            Rectangle{
                id:line_two_id
                width:parent.width *0.8
                height:1
                anchors.bottom: code_text_id.bottom
                anchors.bottomMargin: -10
                anchors.left: parent.left
                anchors.leftMargin: parent.width *0.1
                color: "black"
            }
            LinearGradient{
                anchors.fill:line_two_id
                gradient: Gradient{
                    GradientStop{position: 0.0; color: "white"}
                    GradientStop{position: 0.5; color: "black"}
                    GradientStop{position: 0.99; color: "white"}
                }
                source: line_one_id
                start: Qt.point(0,0)
                end:Qt.point(line_one_id.width,0)
            }
            Rectangle{
                id:getcode_rec_id
                Text{
                    text: "getcode"
                    anchors.centerIn: parent
                    font.pointSize:parent.width *0.1
                    color: "white"
                }

                width: code_text_id.width * 1.5
                height: code_text_id.height
                color: "gray"
                anchors.top: parent.top
                anchors.topMargin: parent.height * 0.5
                anchors.left: parent.left
                anchors.leftMargin: parent.width * 0.7
            }
            DropShadow{
                id:getcode_rec_shadow_id
                anchors.fill: getcode_rec_id
                horizontalOffset: 5
                verticalOffset: 5
                radius: 8
                samples: 17
                color: "#999999"
                source: getcode_rec_id
            }
        }
        Rectangle{
            id:login_rec_id
            width: parent.width*0.5
            height: parent.height*0.05
            Text{
               text: "login"
               font.pixelSize: parent.height *0.5
               anchors.centerIn: parent
               color: "white"
            }
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.height * 0.3
            anchors.horizontalCenter: parent.horizontalCenter
            radius: 50

            color: "black"

        }
        DropShadow{
            id:login_rec_shadow_id
            anchors.fill: login_rec_id
            horizontalOffset: 5
            verticalOffset: 5
            radius: 8
            samples: 17
            color: "#999999"

            source: login_rec_id

        }


        CheckBox{
                text:"next-login-rememberpassword"
                anchors.bottom: parent.bottom
                anchors.bottomMargin: parent.height * 0.25
                anchors.horizontalCenter: parent.horizontalCenter
        }


    }

}
