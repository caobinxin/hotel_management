import QtQuick 2.0

import QtGraphicalEffects 1.0
Rectangle {
    width: parent.width
    height: parent.height
    Text {
        id: name
        text: qsTr("Please scan the code to pay the deposit")
        anchors.top: parent.top
        anchors.topMargin: parent.height *0.15
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 13
    }
    Rectangle{
        id: order_rec_id
        width:300
        height: 300
        color: "black"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: name.top
        anchors.topMargin: 40
    }
    DropShadow{
        anchors.fill: order_rec_id
        horizontalOffset: 7
        verticalOffset: 7
        radius: 8
        samples: 20
        color: "#999999"
        source: order_rec_id
    }
    Rectangle{
        id:order_line_id
        width:parent.width *0.8
        height:1
        anchors.bottom:order_rec_id.bottom
        anchors.bottomMargin:-50
        anchors.horizontalCenter: parent.horizontalCenter
    }
    LinearGradient{
        anchors.fill:order_line_id
        gradient: Gradient{
            GradientStop{position: 0.0; color: "white"}
            GradientStop{position: 0.3; color: "black"}
            GradientStop{position: 0.7; color: "black"}
            GradientStop{position: 1; color: "white"}
        }
        source: order_line_id
        start: Qt.point(0,0)
        end:Qt.point(order_line_id.width,0)
    }
}
