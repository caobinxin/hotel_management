import QtQuick 2.0
import QtGraphicalEffects 1.0

Rectangle {
    width: parent.width
    height: parent.height
    Row{
        width: parent.width
        height: parent.height
        Text {
            id: name
            text: qsTr("请注意")
            anchors.top: parent.top
            anchors.topMargin: parent.height *0.33
            font.pointSize: 30
            anchors.left: order_line_id.left
            anchors.leftMargin:-350
        }
        Text {
            text: qsTr("根据有关部门之规定
入住本店需一人一证
请实名登记本人证件")
            anchors.top: parent.top
            anchors.topMargin: parent.height *0.4
            font.pointSize: 20
            anchors.left: order_line_id.left
            anchors.leftMargin: -400
        }
        Rectangle{
            id:order_line_id
            width:1
            height:order_rec_id.height*1.5
            anchors.top:parent.top
            anchors.topMargin:parent.height *0.1
            anchors.horizontalCenter: parent.horizontalCenter
            gradient: Gradient{
                GradientStop{position: 0.0; color: "white"}
                GradientStop{position: 0.3; color: "black"}
                GradientStop{position: 0.7; color: "black"}
                GradientStop{position: 1; color: "white"}
            }
        }

        Rectangle{
            id: order_rec_id
            width:parent.height * 0.5
            height: parent.height * 0.5
            color: "black"
            anchors.left:order_line_id.left
            anchors.leftMargin: 30
            anchors.top: parent.top
            anchors.topMargin: parent.height *0.2
            Grid{
                width: parent.width
                height: parent.height
                anchors.centerIn: parent
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.1
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.15
                    color: "white"
                    text: qsTr("1")
                    font.pointSize: parent.width * 0.1
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.1
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.45
                    color: "white"
                    text: qsTr("2")
                    font.pointSize: parent.width * 0.1
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.1
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.75
                    color: "white"
                    text: qsTr("3")
                    font.pointSize: parent.width * 0.1
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.4
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.15
                    color: "white"
                    text: qsTr("4")
                    font.pointSize: parent.width * 0.1
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.4
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.45
                    color: "white"
                    text: qsTr("5")
                    font.pointSize: parent.width * 0.1
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.4
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.75
                    color: "white"
                    text: qsTr("6")
                    font.pointSize: parent.width * 0.1
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.7
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.15
                    color: "white"
                    text: qsTr("7")
                    font.pointSize: parent.width * 0.1
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.7
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.45
                    color: "white"
                    text: qsTr("8")
                    font.pointSize: parent.width * 0.1
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.7
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.75
                    color: "white"
                    text: qsTr("9")
                    font.pointSize: parent.width * 0.1
                }

            }
        }
        DropShadow{
            anchors.fill: order_rec_id
            horizontalOffset: 7
            verticalOffset: 7
            radius: 8
            samples: 20
            color: "#999999"
            source: order_rec_id
        }
    }

}
