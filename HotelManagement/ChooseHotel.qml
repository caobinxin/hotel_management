import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import QtGraphicalEffects 1.0

Rectangle{
    width: parent.width
    height: parent.height

    Column{
        width: parent.width
        height: parent.height
        Text {
            id: hander
            text: qsTr("please choose your hotel")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: parent.height * 0.1
        }
        Rectangle{
            id:line_Choose_one_id
            color: "black"
            width:parent.width *0.9
            height:1
            anchors.top: parent.top
            anchors.topMargin: 100
            anchors.horizontalCenter: parent.horizontalCenter
        }
        LinearGradient{
            anchors.fill:line_Choose_one_id
            gradient: Gradient{
                GradientStop{position: 0.0; color: "white"}
                GradientStop{position: 0.2; color: "black"}
                GradientStop{position: 0.99; color: "white"}
            }
            source: line_Choose_one_id
            start: Qt.point(0,0)
            end:Qt.point(line_Choose_one_id.width,0)
        }

        Rectangle{
            id:rec_Choose_one_id
            color: "black"
            width:parent.width * 0.7
            height:parent.height * 0.3
            anchors.top: parent.top
            anchors.topMargin: 150
            anchors.horizontalCenter: parent.horizontalCenter
        }
        DropShadow{
            anchors.fill: rec_Choose_one_id
            horizontalOffset: 10
            verticalOffset: 10
            radius: 8
            samples: 17
            color: "#999999"
            source: rec_Choose_one_id
        }

        Rectangle{
            id:rec_Choose_two_id
            color: "black"
            width:parent.width * 0.7
            height:parent.height * 0.3
            anchors.top: parent.top
            anchors.topMargin: 400
            anchors.horizontalCenter: parent.horizontalCenter
        }
        DropShadow{
            anchors.fill: rec_Choose_two_id
            horizontalOffset: 10
            verticalOffset: 10
            radius: 8
            samples: 17
            color: "#999999"
            source: rec_Choose_two_id
        }
    }
}
