import QtQuick 2.0
import QtGraphicalEffects 1.0

Rectangle {
    width: parent.width
    height: parent.height
    Row{
        width: parent.width
        height: parent.height
        Text {
            id: name
            text: qsTr("请输入订单号后六位")
            anchors.top: parent.top
            anchors.topMargin: parent.height *0.4
            font.pointSize: 15
            anchors.left: order_line_id.left
            anchors.leftMargin:-350
        }
        Rectangle{
            id:line_timeout_id
            width:parent.width *0.3
            height:1
            anchors.bottom: name.bottom
            anchors.bottomMargin: -20
            anchors.horizontalCenter: name.horizontalCenter
        }
        LinearGradient{
            anchors.fill:line_timeout_id
            gradient: Gradient{
                GradientStop{position: 0.0; color: "white"}
                GradientStop{position: 0.2; color: "black"}
                GradientStop{position: 0.8; color: "black"}
                GradientStop{position: 0.99; color: "white"}
            }
            source: line_timeout_id
            start: Qt.point(0,0)
            end:Qt.point(line_timeout_id.width,0)
        }

        Rectangle{
            id:order_line_id
            width:1
            height:order_rec_id.height*1.5
            anchors.top:parent.top
            anchors.topMargin:parent.height *0.1
            anchors.horizontalCenter: parent.horizontalCenter
            gradient: Gradient{
                GradientStop{position: 0.0; color: "white"}
                GradientStop{position: 0.3; color: "black"}
                GradientStop{position: 0.7; color: "black"}
                GradientStop{position: 1; color: "white"}
            }
        }

        Rectangle{
            id: order_rec_id
            width:parent.height * 0.5
            height: parent.height * 0.5
            color: "black"
            anchors.left:order_line_id.left
            anchors.leftMargin: 30
            anchors.top: parent.top
            anchors.topMargin: parent.height *0.2
            Grid{
                width: parent.width
                height: parent.height
                anchors.centerIn: parent
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.1
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.15
                    color: "white"
                    text: qsTr("1")
                    font.pointSize: parent.width * 0.08
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.1
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.45
                    color: "white"
                    text: qsTr("2")
                    font.pointSize: parent.width * 0.08
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.1
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.75
                    color: "white"
                    text: qsTr("3")
                    font.pointSize: parent.width * 0.08
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.35
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.15
                    color: "white"
                    text: qsTr("4")
                    font.pointSize: parent.width * 0.08
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.35
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.45
                    color: "white"
                    text: qsTr("5")
                    font.pointSize: parent.width * 0.08
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.35
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.75
                    color: "white"
                    text: qsTr("6")
                    font.pointSize: parent.width * 0.08
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.6
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.15
                    color: "white"
                    text: qsTr("7")
                    font.pointSize: parent.width * 0.08
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.6
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.45
                    color: "white"
                    text: qsTr("8")
                    font.pointSize: parent.width * 0.08
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.6
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.75
                    color: "white"
                    text: qsTr("9")
                    font.pointSize: parent.width * 0.08
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.82
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.1
                    color: "white"
                    text: qsTr("确认")
                    font.pointSize: parent.width * 0.06
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.83
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.45
                    color: "white"
                    text: qsTr("0")
                    font.pointSize: parent.width * 0.08
                }
                Text {
                    anchors.top: parent.top
                    anchors.topMargin: parent.width * 0.82
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width * 0.7
                    color: "white"
                    text: qsTr("取消")
                    font.pointSize: parent.width * 0.06
                }

            }
        }
        DropShadow{
            anchors.fill: order_rec_id
            horizontalOffset: 7
            verticalOffset: 7
            radius: 8
            samples: 20
            color: "#999999"
            source: order_rec_id
        }
    }

}
