import QtQuick 2.0

import QtGraphicalEffects 1.0

Rectangle {
    width: parent.width
    height: parent.height
    Rectangle{
        id:timeout_rec_id
        width: parent.width *0.5
        height: parent.height *0.5
        color: "black"
        radius: 1000
        anchors.centerIn: parent
        Text {
            text: qsTr("ID Confirmed")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: parent.height *0.35
            font.pointSize: parent.width *0.05
            color: "white"
        }
        Text {
            text: qsTr("Please get your card back")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: parent.height *0.48
            font.pointSize: parent.width *0.05
            color: "white"
        }

    }
    DropShadow{
        anchors.fill: timeout_rec_id
        horizontalOffset: 10
        verticalOffset: 10
        radius: 15
        samples: 30
        color: "gray"
        source: timeout_rec_id
    }
    Rectangle{
        id:line_timeout_id
        width:parent.width *0.8
        height:1
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height*0.15
        anchors.horizontalCenter: parent.horizontalCenter

    }
    LinearGradient{
        anchors.fill:line_timeout_id
        gradient: Gradient{
            GradientStop{position: 0.0; color: "white"}
            GradientStop{position: 0.2; color: "black"}
            GradientStop{position: 0.8; color: "black"}
            GradientStop{position: 0.99; color: "white"}
        }
        source: line_timeout_id
        start: Qt.point(0,0)
        end:Qt.point(line_timeout_id.width,0)
    }

}
