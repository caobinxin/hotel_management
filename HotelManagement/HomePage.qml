import QtQuick 2.0

Rectangle {
    width: parent.width
    height: parent.height

    Row{
        width: parent.width
        height: parent.height

            Column{
                width: parent.width * 0.7
                height: parent.height
                Rectangle {
                    id: handle_hotel_id
                    anchors.right: parent.right
                    anchors.rightMargin: parent.width * 0.2
                    anchors.top: parent.top
                    anchors.margins: parent.height * 0.15

                    color: "#000000"
                    width: parent.width * 0.6
                    height: parent.width * 0.3
                    Text {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: qsTr("办理入住")
                        textFormat: Text.PlainText
                        font.pointSize: parent.parent.parent.width * 0.03
                        color: "#ffffff"
                    }
                }

                Rectangle{
                    id: row_line_id
                    anchors.top: handle_hotel_id.bottom
                    anchors.topMargin: parent.height * 0.04
                    height: 2
                    color: "#000000"
                    width: parent.width
                }

                Row {
                    width: parent.width
                    height: parent.height
                    anchors.top: row_line_id.bottom
                    anchors.topMargin: parent.height * 0.04
                    Text {
                        anchors.right: rq_code_id.left
                        anchors.rightMargin: parent.width * 0.04
                        anchors.verticalCenter: rq_code_id.verticalCenter

                        font.pointSize: parent.width * 0.03
                        text: qsTr("还没下单？\n扫码去预定")

                    }

                    Rectangle {
                        id: rq_code_id
                        anchors.right: parent.right
                        anchors.rightMargin: parent.width * 0.2
                        color: "#000000"
                        width: parent.width * 0.3
                        height: parent.width * 0.3
                    }
                }
            }


        Rectangle{
            id: column_line_id
            height: parent.height
            color: "#000000"
            width: 3
        }

        Text {
            anchors.verticalCenter: parent.verticalCenter
            text: qsTr("欢迎您使用AINN自助入住")
            font.pointSize: parent.width * 0.02
        }
    }
}
