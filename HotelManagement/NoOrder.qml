import QtQuick 2.0
import QtGraphicalEffects 1.0

Rectangle {
    width: parent.width
    height: parent.height
    Column{
        width: parent.width
        height: parent.height
        Row{
            width: parent.width
            height: parent.height
            Text {
                id:matching_text_id
                text: qsTr("未匹配到该身份证的订单，请输入订单预留姓名")
                font.pointSize: parent.width *0.015
                anchors.top: parent.top
                anchors.topMargin: parent.height *0.3
                anchors.left: parent.left
                anchors.leftMargin: parent.width *0.15
            }
            Rectangle{
                width: parent.width*0.08
                height:parent.height *0.05
                id:clear_rec_NoOrder_id
                anchors.left: matching_text_id.right
                anchors.leftMargin: 50
                anchors.bottom: line_NoOrder_id.top
                anchors.bottomMargin: 10
                color: "black"
                Text {
                    anchors.centerIn: parent
                    text: qsTr("清除")
                    color: "white"
                    font.pointSize: parent.width * 0.2
                }
            }
            DropShadow{
                anchors.fill: clear_rec_NoOrder_id
                horizontalOffset: 10
                verticalOffset: 10
                radius: 15
                samples: 30
                color: "gray"
                source: clear_rec_NoOrder_id
            }
            Rectangle{
                width: parent.width*0.08
                height:parent.height *0.05
                id:confirm_rec_NoOrder_id
                anchors.left: clear_rec_NoOrder_id.right
                anchors.leftMargin: 10
                anchors.bottom: line_NoOrder_id.top
                anchors.bottomMargin: 10
                color: "black"
                Text {
                    anchors.centerIn: parent
                    text: qsTr("确认")
                    color: "white"
                    font.pointSize: parent.width * 0.2
                }
            }
            DropShadow{
                anchors.fill: confirm_rec_NoOrder_id
                horizontalOffset: 10
                verticalOffset: 10
                radius: 15
                samples: 30
                color: "gray"
                source: confirm_rec_NoOrder_id
            }

            Rectangle{
                id:line_NoOrder_id
                width:parent.width *0.9
                height:1
                anchors.bottom: matching_text_id.bottom
                anchors.bottomMargin: -10
                anchors.horizontalCenter: parent.horizontalCenter

            }
            LinearGradient{
                anchors.fill:line_NoOrder_id
                gradient: Gradient{
                    GradientStop{position: 0.0; color: "white"}
                    GradientStop{position: 0.1; color: "black"}
                    GradientStop{position: 0.9; color: "black"}
                    GradientStop{position: 0.99; color: "white"}
                }
                source: line_NoOrder_id
                start: Qt.point(0,0)
                end:Qt.point(line_NoOrder_id.width,0)
            }
       }
       Row{
           width: parent.width
           height: parent.height
           anchors.left: parent.left
           anchors.top: parent.top
           Rectangle{
               id:code_rec_id
               width: parent.width * 0.15
               height: parent.width * 0.15
               color: "black"
               anchors.left: parent.left
               anchors.leftMargin: parent.width * 0.15
               anchors.top: parent.top
               anchors.topMargin:parent.height * 0.45
           }
           Text {
               id: name
               text: qsTr("还没下单？
扫码去预定")
               color: "black"
               font.pointSize: 25
               anchors.left: parent.left
               anchors.leftMargin: parent.width * 0.35
               anchors.top: parent.top
               anchors.topMargin:parent.height * 0.5
           }
           Rectangle{
               width: parent.width *0.1
               height: parent.height*0.05
               color: "black"
               anchors.left: parent.left
               anchors.leftMargin: parent.width * 0.6
               anchors.top: parent.top
               anchors.topMargin:parent.height * 0.7
               Text {
                   text: qsTr("手写输入")
                   anchors.centerIn: parent
                   color: "white"
                   font.pointSize: parent.width *0.1
               }

           }
        }
    }
}
